package ru.tsc.karbainova.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.karbainova.tm.event.ConsoleEvent;
import ru.tsc.karbainova.tm.listener.AbstractListenerProject;
import ru.tsc.karbainova.tm.listener.TerminalUtil;

@Component
public class ProjectCreateListener extends AbstractListenerProject {
    @Override
    public String name() {
        return "create-project";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Create project";
    }

    @Override
    @EventListener(condition = "@projectClearListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        final String description = TerminalUtil.nextLine();
        projectEndpoint.createProjectAllParam(sessionService.getSession(), name, description);
        System.out.println("[OK]");
    }
}
