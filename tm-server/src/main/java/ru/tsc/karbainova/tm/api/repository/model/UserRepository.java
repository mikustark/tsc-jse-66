package ru.tsc.karbainova.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.tsc.karbainova.tm.model.User;

public interface UserRepository extends JpaRepository<User, String> {
    void clear();

    @Nullable User findByLogin(@NotNull String login);

    void removeById(@Nullable String id);

    void removeByLogin(@Nullable String login);
}
