package ru.tsc.karbainova.tm.endpoint;

import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.karbainova.tm.api.service.dto.IAdminUserService;
import ru.tsc.karbainova.tm.dto.SessionDTO;
import ru.tsc.karbainova.tm.dto.UserDTO;
import ru.tsc.karbainova.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Component
@WebService
@NoArgsConstructor
public class AdminUserEndpoint extends AbstractEndpoint {

    @NotNull
    @Autowired
    protected IAdminUserService userService;


    @WebMethod
    public void clearUser(
            @WebParam(name = "session") final SessionDTO session
    ) {
        sessionService.validate(session, Role.ADMIN);
        userService.clear();
    }

    @WebMethod
    public void signOutByUserId(
            @WebParam(name = "session") final SessionDTO session) {
        sessionService.validate(session, Role.ADMIN);
        sessionService.signOutByUserId(session.getUserId());
    }

    @WebMethod
    public void addAllUser(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "users") List<UserDTO> users) {
        sessionService.validate(session, Role.ADMIN);
        userService.addAll(users);
    }

    @WebMethod
    public List<UserDTO> findAllUser(@WebParam(name = "session") final SessionDTO session) {
        sessionService.validate(session, Role.ADMIN);
        return userService.findAll();
    }

    @WebMethod
    public UserDTO findByLoginUser(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "login") @NonNull String login
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.findByLogin(login);
    }

    @WebMethod
    public UserDTO removeUser(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "user") UserDTO user
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.removeUser(user);
    }

    @WebMethod
    public UserDTO lockUserByLoginUser(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "login") @NonNull String login
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.lockUserByLogin(login);
    }

    @WebMethod
    public UserDTO unlockUserByLoginUser(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "login") @NonNull String login
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.unlockUserByLogin(login);
    }
}
