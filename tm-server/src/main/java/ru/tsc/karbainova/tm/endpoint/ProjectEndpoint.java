package ru.tsc.karbainova.tm.endpoint;

import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.karbainova.tm.api.service.dto.IProjectService;
import ru.tsc.karbainova.tm.api.service.dto.IProjectToTaskService;
import ru.tsc.karbainova.tm.dto.ProjectDTO;
import ru.tsc.karbainova.tm.dto.SessionDTO;
import ru.tsc.karbainova.tm.dto.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Component
@WebService
@NoArgsConstructor
public class ProjectEndpoint extends AbstractEndpoint {

    @NotNull
    @Autowired
    protected IProjectService projectService;
    @NotNull
    @Autowired
    protected IProjectToTaskService projectToTaskService;


    @WebMethod
    public void addProject(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "project") @NonNull ProjectDTO project) {
        sessionService.validate(session);
        projectService.add(project);
    }

    @WebMethod
    public List<ProjectDTO> findAllProject(
            @WebParam(name = "session") final SessionDTO session
    ) {
        sessionService.validate(session);
        return projectService.findAll();
    }

    @WebMethod
    public void createProject(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "name") @NonNull String name
    ) {
        sessionService.validate(session);
        projectService.create(session.getUserId(), name);
    }

    @WebMethod
    public void createProjectAllParam(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "name") @NonNull String name,
            @WebParam(name = "description") @NonNull String description) {
        sessionService.validate(session);
        projectService.create(session.getUserId(), name, description);
    }

    @WebMethod
    public void removeProject(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "project") @NonNull ProjectDTO project) {
        sessionService.validate(session);
        projectService.remove(session.getUserId(), project);
    }

//    @WebMethod
//    public List<Project> findAllProjectByUs(
//            @WebParam(name = "session") final Session session
//    ) {
//        sessionService.validate(session);
//        return projectService.findAll(session.getUserId());
//    }

    @WebMethod
    public ProjectDTO findByNameProject(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "name") @NonNull String name) {
        sessionService.validate(session);
        return projectService.findByName(session.getUserId(), name);
    }

    @WebMethod
    public List<TaskDTO> findTaskByProjectIdProject(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "projectId") @NonNull String projectId) {
        sessionService.validate(session);
        return projectToTaskService.findTasksByProjectId(session.getUserId(), projectId);
    }

    @WebMethod
    public void taskBindByIdProject(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "projectId") @NonNull String projectId,
            @WebParam(name = "taskId") @NonNull String taskId) {
        sessionService.validate(session);
        projectToTaskService.bindTaskByProjectId(session.getUserId(), projectId, taskId);
    }

    @WebMethod
    public void taskUnbindByIdProject(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "projectId") @NonNull String projectId,
            @WebParam(name = "taskId") @NonNull String taskId) {
        sessionService.validate(session);
        projectToTaskService.unbindTaskByProjectId(session.getUserId(), projectId, taskId);
    }

    @WebMethod
    public void removeProjectById(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "projectId") @NonNull String projectId) {
        sessionService.validate(session);
        projectToTaskService.removeProjectById(projectId);
    }
}
