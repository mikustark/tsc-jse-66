package ru.tsc.karbainova.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.enumerated.Status;
import ru.tsc.karbainova.tm.listener.JpaEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_task")
@EntityListeners(JpaEntityListener.class)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Task extends AbstractOwnerEntity {
    @Column
    @NonNull
    private String name;
    @Column
    @Nullable
    private String description;
    @Nullable
    @Column(name = "project_id")
    private String projectId = null;
    @NonNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;
    @Nullable
    @Column(name = "start_date")
    private Date startDate;
    @Nullable
    @Column(name = "finish_date")
    private Date finishDate;
    @NonNull
    @Column
    private Date created = new Date();
    @Nullable
    @ManyToOne
    private Project project;
}
