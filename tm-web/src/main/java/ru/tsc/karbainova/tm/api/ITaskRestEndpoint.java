package ru.tsc.karbainova.tm.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import ru.tsc.karbainova.tm.model.Task;

import java.util.List;

public interface ITaskRestEndpoint {
    @GetMapping("/findAll")
    List<Task> findAll();

    @GetMapping("/find/{id}")
    Task find(@PathVariable("id") String id);

    @PostMapping("/create")
    Task create(@RequestBody Task task);

    @PostMapping("/save")
    Task save(@RequestBody Task task);

    @PostMapping("/delete/{id}")
    void delete(@PathVariable("id") String id);

}
