package ru.tsc.karbainova.tm.api.service;

import org.springframework.stereotype.Service;
import ru.tsc.karbainova.tm.model.Project;

import java.util.Collection;
import java.util.List;

public interface IProjectService {
    List<Project> findAll();

    void addAll(Collection<Project> collection);

    Project save(Project entity);

    void create();

    Project findById(String id);

    void clear();

    void removeById(String id);

    void remove(Project entity);
}
