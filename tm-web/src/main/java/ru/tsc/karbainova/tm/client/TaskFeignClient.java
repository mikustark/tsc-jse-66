package ru.tsc.karbainova.tm.client;

import feign.Feign;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import ru.tsc.karbainova.tm.api.ITaskRestEndpoint;
import ru.tsc.karbainova.tm.model.Task;

import java.util.List;

public interface TaskFeignClient extends ITaskRestEndpoint {
    String URL = "http://localhost:8080/api/tasks";

    static TaskFeignClient client() {
        final FormHttpMessageConverter converter =
                new FormHttpMessageConverter();
        final HttpMessageConverters converters =
                new HttpMessageConverters(converter);
        final ObjectFactory<HttpMessageConverters> objectFactory =
                () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(TaskFeignClient.class, URL);
    }

    @GetMapping("/findAll")
    List<Task> findAll();

    @GetMapping("/find/{id}")
    Task find(@PathVariable("id") final String id);

    @PostMapping("/create")
    Task create(@RequestBody final Task task);

    @PutMapping("/save")
    Task save(@RequestBody final Task task);

    @DeleteMapping("/delete/{id}")
    void delete(@PathVariable("id") final String id);

}
