package ru.tsc.karbainova.tm.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("ru.tsc.karbainova.tm")
public class ApplicationConfig {
}
