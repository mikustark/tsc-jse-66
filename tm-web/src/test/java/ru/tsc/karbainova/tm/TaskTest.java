package ru.tsc.karbainova.tm;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.karbainova.tm.api.ITaskRestEndpoint;
import ru.tsc.karbainova.tm.client.TaskFeignClient;
import ru.tsc.karbainova.tm.marker.RestCategory;
import ru.tsc.karbainova.tm.model.Task;

public class TaskTest {

    final Task task1 = new Task("test task 1");

    final Task task2 = new Task("test task 2");

    final ITaskRestEndpoint client = TaskFeignClient.client();

    @Before
    public void before() {
        client.create(task1);
    }

    @After
    public void after() {
    }

    @Test
    @Category(RestCategory.class)
    public void find() {
        Assert.assertEquals(task1.getName(), client.find(task1.getId()).getName());
    }

    @Test
    @Category(RestCategory.class)
    public void create() {
        Assert.assertNotNull(client.create(task1));
    }

    @Test
    @Category(RestCategory.class)
    public void update() {
        final Task updatedTask = client.find(task1.getId());
        updatedTask.setName("updated");
        client.save(updatedTask);
        Assert.assertEquals("updated", client.find(task1.getId()).getName());
    }

    @Test
    @Category(RestCategory.class)
    public void delete() {
        client.delete(task1.getId());
        Assert.assertNull(client.find(task1.getId()));
    }

    @Test
    @Category(RestCategory.class)
    public void findAll() {
        Assert.assertEquals(1, client.findAll().size());
        client.create(task2);
        Assert.assertEquals(2, client.findAll().size());
    }

}
